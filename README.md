# apache cordova [ tools ] update 6 with angular 2 support

I found there to be a lack of angular 2 [ng2] references on how to make it work in the context of current visual studio 2015 update 1 apache cordova 
[ tools ] update 6 project environment.  So i started with what i found to be relevant for making it work in current generation asp.net 
[ execution environment, aka dnx ] 5 web app environment, see [https://github.com/myusrn/dnx5withng2](https://github.com/myusrn/dnx5withng2) and iterated on
issues i hit with people who understood the apache cordova typescript based project template and angular 2 bootstraping. The project contained in this repo 
is intended as a guide for what i found was necessary to make things in apache cordova [ tools ] update 6 typescript environment as that is the target 
environment i wanted to work from with my angular 2 exploration.

using vs15upd1 support for apache cordova, aka taco, see [http://taco.visualstudio.com/en-us/docs/create-a-hosted-app/](http://taco.visualstudio.com/en-us/docs/create-a-hosted-app/)  
and if vs15upd1 notifications has yet ot offer you the apache cordova tools update 6 get it at [http://microsoft.github.io/vstacoblog/2016/02/04/announcing-update-6.html](http://microsoft.github.io/vstacoblog/2016/02/04/announcing-update-6.html)  

install node.js and afterwards add %appdata%\npm to your sysdm.cpl | environment variables | user variables | path
npm install -g cordova

things i did to get apache cordova, aka taco, project to support ng2 bootstrapping and adal [ + graph api ] cordova plugin working  
- tsconfig.json settings for relevant typescript transpiling, using $(ProjectDir) located version to enable npm typescript -watch mode support that doesn't
work if using $(ProjectDir)scripts [ or app ] folder located settings file
- tsconfig.json "inlineSources": true is cordova project default to enable chrome ripple debug resolution of original sources, switch to false to enable
visual studio f5 debug/test using device emulator targets to support variable hover over value inspection support and step through using original source   
- bower.json for bower_components and package.json for node_modules  
- add gulpfile.js to enable clean enlistment, and CI pre/post build, task runner execution of copy[:npmjs] that moves relevant node_module scripts into www/node_modules  
- injected necessary ng2 script references into index.html and necessary System.register and .import calls into index.ts onDeviceReady handler  
- config.xml | windows | target version | 8.1 -&gt; 10.0 == &lt;preference name="windows-target-version" value="8.1" -&gt; "10.0" /&gt;  
- repo .gitignore settings to keep typescript transpiler entries from showing up in pending changes where you then have to manually exclude them

\- wrt fix involving placying System.config and the System.import calls in OnDeviceReady handler, vs being able to have them in index.html script block, 
the reason is due to the security model of the cordova [ / uwp javascript ] app sandbox, inline &lt;script /&gt; execution is explicitly disabled.  If 
you want more details it is documented here [https://cordova.apache.org/docs/en/dev/guide/platforms/win8/win10-support.html](https://cordova.apache.org/docs/en/dev/guide/platforms/win8/win10-support.html).
Essentially Cordova project is a website, if you don't need to use sensors. Most of the develop work will be spent on dealing with the java/typescript
logic and html/css assets. If this part can be addressed first with a quicker way, then sweep out the integration issues with real emulator or device, 
it is pretty economical. Sensors can be easily mocked up temporarily during this workflow. That's actualy what Ripple does. There are scenarios where 
f12 debug tools are the most optimal way to go.  Using typescript to generate that javascript doesn't rule out using f12 dev tools for debugging and
testing changes when its desired. Currently our support for typescript debugging through vs15 is the best.

\- current only a typscript compiler will look for tsconfig.json in $(ProjectDir), $(ProjectDir)scripts, $(ProjectDir)&lt;other&gt; and process using 
settings from the first one it hits thus the reason why apache cordova app files were placed in $(ProjectDir)scripts instead of $(ProjectDir)apps.  At
some point down the road i'm told the typescript compiler will support multiple tsconfig.json files per project at which point you can maintain settings
unique to folder specific *.ts sources independently and not have to come up with a merged settings file that works for all.

\- you can alias imported module/class names when using from with the following syntax  
import { StringUtils as StrUtl1 } from './string-utils';  
import * as StrUtl2 from './string-utils';  
export let StrUtl3 = StrUtl2;  
for more details on file/module/class exports see [http://stackoverflow.com/questions/30357634/how-do-i-use-namespaces-with-typescript-external-modules](http://stackoverflow.com/questions/30357634/how-do-i-use-namespaces-with-typescript-external-modules) 
and [http://www.typescriptlang.org/Handbook#modules-pitfalls-of-modules](http://www.typescriptlang.org/Handbook#modules-pitfalls-of-modules)  

\- whatever module target you specify, you expect that loader to exist at runtime.  If you use CommonJS, you expect a node/CommonJS-like loader e.g. 
browserify/webpack to be involved, requireJs for amd, or systemjs for system. Use of --module es6, assumes the underlying system support modules natively, 
which does not exist today. For more details see [https://github.com/Microsoft/TypeScript-Handbook/blob/master/pages/Modules.md](https://github.com/Microsoft/TypeScript-Handbook/blob/master/pages/Modules.md).  

\- to convert an apache cordova tools update 5 created project to update 6 compatible project, that supports es6/es2015 transpiler output, you need to 
remove the following line from the top of your jsproj file.  for more details see [https://github.com/Microsoft/TypeScript/issues/6558](https://github.com/Microsoft/TypeScript/issues/6558).  

\- typescript async/await and promises references based on "typescript new promise resolve" hits
[http://stackoverflow.com/questions/24555636/how-do-i-declare-a-generic-promise-in-typescript-so-that-when-the-generic-type](http://stackoverflow.com/questions/24555636/how-do-i-declare-a-generic-promise-in-typescript-so-that-when-the-generic-type)
[https://blogs.msdn.microsoft.com/typescript/2015/11/03/what-about-asyncawait/](https://blogs.msdn.microsoft.com/typescript/2015/11/03/what-about-asyncawait/)  
[https://www.wipdeveloper.com/2015/12/03/typescript-whats-new-in-1-7/](https://www.wipdeveloper.com/2015/12/03/typescript-whats-new-in-1-7/)

\- cordova ng2 oauth alternative to oob adal cordova plugin is can be found at [https://github.com/nraboy/ng2-cordova-oauth(https://github.com/nraboy/ng2-cordova-oauth)
with more from the author of it at [https://blog.nraboy.com/2016/01/use-ng2-cordova-oauth-for-all-your-ionic-2-oauth-needs/](https://blog.nraboy.com/2016/01/use-ng2-cordova-oauth-for-all-your-ionic-2-oauth-needs/)
and also seems like [http://github.com/auth0/angular2-jwt/](http://github.com/auth0/angular2-jwt/) -> [http://github.com/auth0/ng2-jwt/](http://github.com/auth0/ng2-jwt/)
is another alternative

\- references on using ng2 http abstraction in lieu of XMLHTTPRequest [xhr] and jquery.ajax() [ / $.ajax(), asynchoronmous javascript and xml/json ]
[http://stackoverflow.com/questions/35254579/angular-2-http-post](http://stackoverflow.com/questions/35254579/angular-2-http-post)  
[http://stackoverflow.com/questions/34755350/using-http-rest-apis-with-angular-2/34758630#34758630](http://stackoverflow.com/questions/34755350/using-http-rest-apis-with-angular-2/34758630#34758630)  
[http://stackoverflow.com/questions/35370405/angular-2-http-cannot-resolve-all-parameters-for-appservice](http://stackoverflow.com/questions/35370405/angular-2-http-cannot-resolve-all-parameters-for-appservice)  
[http://stackoverflow.com/questions/35350130/error-using-http-json-angularjs-2/35350172#35350172](http://stackoverflow.com/questions/35350130/error-using-http-json-angularjs-2/35350172#35350172)

\- for ng2 support for jwt injection, expiration checking, decoding use "npm install ng2-jwt" [https://github.com/jkuri/ng2-jwt](https://github.com/jkuri/ng2-jwt)
which is fork of "npm install angular2-jwt" [https://github.com/auth0/angular2-jwt/](https://github.com/auth0/angular2-jwt/) that addresses systemjs vs
commonjs/requires basd module enviroment matter discussed here [https://github.com/auth0/angular2-jwt/issues/36](https://github.com/auth0/angular2-jwt/issues/36)
and a supperset of "npm install jwt-decode" [https://github.com/auth0/jwt-decode](https://github.com/auth0/jwt-decode)

\- the target plaform browser determines the html/css/ecmascript engine that will be in effect, i.e. android uses chromium, ios uses webkit, windows 10 
uses edge and windows 8.1 uses ie/trident.  you can use [https://crosswalk-project.org/](https://crosswalk-project.org/) to enable cordova app output 
compatibility across android's non-evergreen browser versions, thereby simplifying your test matrix and guaranteeing long-term support for a fixed version 
number. This comes at the cost of up to 50mb additional package size but testing has shown that crosswalk has a negligible impact on app startup time. The
ios and windows targets do not allow you to package your own rendering engine, so they will always be evergreen.

\- for vs15upd cordovaUpd6 source control see [http://taco.visualstudio.com/en-us/docs/general/#what-to-add-to-source-control](http://taco.visualstudio.com/en-us/docs/general/#what-to-add-to-source-control)
and [http://taco.visualstudio.com/en-us/docs/tips-and-workarounds-general-readme/#building-a-cordova-project-from-source-control-results-in-a-successful-build-but-with-cordova-plugin-apis-not-returning-results-when-the-app-is-run](http://taco.visualstudio.com/en-us/docs/tips-and-workarounds-general-readme/#building-a-cordova-project-from-source-control-results-in-a-successful-build-but-with-cordova-plugin-apis-not-returning-results-when-the-app-is-run)

\- for vs15upd cordovaUpd6 continuous integration [ci] see [https://marketplace.visualstudio.com/items?itemName=ms-vsclient.cordova-extension](https://marketplace.visualstudio.com/items?itemName=ms-vsclient.cordova-extension)
for android and windows coverage and see [http://www.macincloud.com/pricing/build-agent-plans/vso-build-agent-plan](http://www.macincloud.com/pricing/build-agent-plans/vso-build-agent-plan) for ios coverage 

\- for vs15 cordovaUpd6 continuous deployment [cd] see [https://www.npmjs.com/package/cordova-plugin-code-push](https://www.npmjs.com/package/cordova-plugin-code-push)
and [https://github.com/Microsoft/cordova-plugin-code-push](https://github.com/Microsoft/cordova-plugin-code-push)

\- compatibility table on ecmascript engine support for es6 [https://kangax.github.io/compat-table/es6/](https://kangax.github.io/compat-table/es6/)
and upcoming typescript release that provides es6 functionality in es5 output [https://blogs.msdn.microsoft.com/typescript/2016/01/28/announcing-typescript-1-8-beta/](https://blogs.msdn.microsoft.com/typescript/2016/01/28/announcing-typescript-1-8-beta/).
An interim es5 engine compatible story for use of typescript es6 features see "babel regenerator to transpile your typescript es6 output into es5" -> 
[http://weblogs.asp.net/dwahlin/getting-started-with-es6-%E2%80%93-transpiling-es6-to-es5](http://weblogs.asp.net/dwahlin/getting-started-with-es6-%E2%80%93-transpiling-es6-to-es5) 
and [https://github.com/cveld/AsyncAwait-TypeScript-Example](https://github.com/cveld/AsyncAwait-TypeScript-Example) which i tried and found there are still
issues when systemjs module loader attempts to bootstrap ng2 which all should go away when typescript vNext supports using es6 based features with es5 output.
Also didn't like use of these transpilers as they break the oob typescript generation of *.js.map files that enable stopping on typescript soft break points
```
npm install gulp-babel --save-dev  
npm install gulp-traceur --save-dev  
//npm install gulp-typescript --save-dev  // optional
npm install gulp-plumber --save-dev  // optional but can help fix errors that occur in the gulp pipeline
//npm install gulp-concat --save-dev  // optional and already in place  
//npm install gulp-uglify --save-dev  // optional and already in place
gulpfile.js | es6-to-es5-babel, es6-to-es5-traceur and es6-watch tasks
```
