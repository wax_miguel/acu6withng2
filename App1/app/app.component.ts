﻿import {Component, Inject, View} from 'angular2/core';
import {CONFIG, Config} from './app.config';
//import {Http, ConnectionBackend} from 'angular2/http'; // or HTTP_PROVIDERS which has all defined
import {RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
//import {Observable} from 'rxjs/Rx';
import 'rxjs/Rx';  // or import that adds all operators to Observable
import {AuthService} from './auth.service';
import {Core} from './core';

//export let APP_CONFIG = new OpaqueToken('app.config');

@Component({
    selector: 'my-app',
    //template: '<h1>My Apache Cordova Angular 2 App</h1>',
    //template: '<h1>Welcome page for {{message}}</h1>',
    template: `
    <button id='signin' (click)='onSignInClick()'>sign-in</button>&nbsp;&nbsp;{{signinMessage}}<br >
    <button id='test' (click)='onTestClick()'>test</button>&nbsp;&nbsp;{{testMessage}}`,
    //providers: [<list of depedency injection providers you want to override or set at this component scope>]
})
export class AppComponent {

    //message: string;
    signinMessage = '';
    testMessage = '';
    //jwtHelper = new JwtHelper();

    constructor(@Inject('app.config') private config: Config, /* private http: Http, */ private authService: AuthService) { 
    //constructor(@Inject(APP_CONFIG) private config: Config, /* private http: Http, */ private authService: AuthService) {
        //this.message = 'My Apache Cordova Angular 2 App';
        let test = this.config.test;
    }
    
    ///* async */ onSignInClick() {  // this syntax defines click handler as method which sets up "var _this = this;" method variable
    /* async */ onSignInClick = () => {  // this syntax defines click handler as member function which sets up "var _this = this;" member variable
        var buttonDisabled = document.getElementById('signin').getAttribute('disabled');
        //var buttonDisabled = document.querySelector('button.signinCss').getAttribute('disabled')

        document.getElementById('signin').setAttribute('disabled', 'disabled');
        //document.querySelector('button.signinCss').setAttribute('disabled', 'disabled');

        this.signinMessage = 'starting signin . . .';
        //let testString = this.config.test;

        // typescript es6 async/await, covered by typescript 2.0 target: "es5" output, based call
        //var accessToken = await new AuthService(this.config, this.http).getAccessToken();

        // typescript es6 promise, covered by evergreen browsers and core-js/es6-shim elsewhere, based call
        this.authService.getAccessToken().then(accessToken => {
            //let jwt = this.jwtHelper.decodeToken(accessToken);
            //let jwt = new JwtHelper().decodeToken(accessToken);
            let jwt = Core.decodeAccessToken(accessToken);
            this.signinMessage = 'success';  // note that hover over will show undefined but transpiler created and is using valid _this reference for it
            document.getElementById('signin').innerText = 'sign-out';
            document.getElementById('signin').removeAttribute('disabled');  // not setAttribute('disabled', null);
            //document.querySelector('button.signinCss').removeAttribute('disabled');  // not setAttribute('disabled', null);
        }, err => {  // use err instanceof <type> and (err.property === undefined) and !(err.property === undefined) to check type and properties
            console.error('landed in app.component outer promise rejected handler, see output window for details')
        });
    }

    ///* async */ onTestClick() {  // this syntax defines click handler as method which sets up "var _this = this;" method variable
    /* async */ onTestClick = () => {  // this syntax defines click handler as member function which sets up "var _this = this;" member variable
        this.testMessage = 'starting test . . .';

        //let result = await this.ping();
        //this.testMessage = 'success';

        this.ping().then(result => {
            this.testMessage = 'success';
        });        
    }

    /* async */ ping(): Promise<number> {
        return new Promise<number>(resolve => { 
            for (var i = 0; i < 5; i++) {
                //await this.delay(300);
                this.delay(300).then(() => { var test = 1; });
                console.log("ping" + i);
            }
            //return i;
            resolve(i);
        });
    }

    delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}