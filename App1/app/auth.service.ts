﻿import {Injectable as Service, Inject, OpaqueToken} from 'angular2/core';
import {CONFIG, Config} from './app.config';
import {Http, Headers, Request, RequestMethod, RequestOptions, Response, ResponseOptions, URLSearchParams} from 'angular2/http';
import {Core} from './core';

"use strict";

//declare var APP_CONFIG: OpaqueToken;  // declaring a global external symbol defined in main.ts
declare var Microsoft: any;  // declaring a global external symbol defined in adal plugin
    
@Service()  // using @Injectable alias for code readability reasons discussed here https://github.com/angular/angular/issues/4404
export class AuthService {
    
    //someInstanceMember: string;
    //someInstanceMemberFn: () => string;

    constructor(@Inject('app.config') private config: Config, private http: Http) { 
    //constructor(@Inject(APP_CONFIG) private config: Config, private http: Http) {
        //this.someInstanceMember = "some instance member value";
        //this.someInstanceMemberFn = () => { /* do some stuff */ return "some instance member function result"; }
        let test = config.test;
    }

    // derived from https://github.com/Azure-Samples/active-directory-cordova-multitarget | platforms\windows\www\js\index.ts | authenticate: function
    // and https://github.com/Azure-Samples/active-directory-cordova-graphapi | platforms\windows\www\js\services\aad-client.svc.js | function authenticate
    // and https://github.com/AzureAD/azure-activedirectory-library-for-cordova/blob/master/sample/js/index.js 
    // and http://stackoverflow.com/questions/14173228/wait-until-promise-and-nested-thens-are-complete
    public /* async */ getAccessToken(): Promise<string> {
        if (!Core.isBrowserEmulator()) {
            // typescript es6 async/await, covered by typescript 2.0 target: "es5" output, based call
            //let ac = new Microsoft.ADAL.AuthenticationContext(this.config.authority);
            //try {  // attempt to get token from cache and if expired acquire a new one using refresh token
            //    let ar = await ac.acquireTokenSilentAsync(this.config.resourceUri, this.config.clientId);
            //    return ar.accessToken;
            //}
            //catch (ex) {
            //    if (ex.ErrorCode == "failed_to_acquire_token_silently") {  // attempt to get new token using interactive signin
            //        let ar = await ac.acquireTokenAsync(this.config.resourceUri, this.config.clientId, this.config.redirectUri);
            //        return ar.accessToken;
            //    }
            //}

            // typescript es6 promise, covered by evergreen browsers and core-js/es6-shim elsewhere, based call
            let ac = new Microsoft.ADAL.AuthenticationContext(this.config.authority);
            return ac.acquireTokenSilentAsync(this.config.resourceUri, this.config.clientId)
            .then(ar => ar, err => ac.acquireTokenAsync(this.config.resourceUri, this.config.clientId, this.config.redirectUri))
            .then(ar => ar.accessToken); // err.message.contains('Call method AcquireToken') when cache miss or expired refreshToken
        } else {
            // typescript es6 async/await, covered by typescript 2.0 target: "es5" output, based call
            //return await Task.FromResult<string>(this.config.testAadAccessToken);

            // typescript es6 promise, covered by evergreen browsers and core-js/es6-shim elsewhere, based call
            //return new Promise<string>(resolve => { resolve(this.config.testAadAccessToken); });
            return Promise.resolve(this.config.testAadAccessToken);
        }
    }
}
