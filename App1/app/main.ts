﻿/// <reference path="../scripts/typings/cordova/cordova.d.ts" />  // enables build with window.cordova and related references

import {bootstrap} from 'angular2/platform/browser'
import {/* OpaqueToken, */ provide} from 'angular2/core';
import {CONFIG, Config} from './app.config';
import {HTTP_PROVIDERS} from 'angular2/http'
import {ROUTER_PROVIDERS} from 'angular2/router';
import {AppComponent} from './app.component'
import {AuthService} from './auth.service';

//export let APP_CONFIG = new OpaqueToken('app.config');

//bootstrap(AppComponent);
bootstrap(AppComponent, [provide('app.config', { useValue: CONFIG }), HTTP_PROVIDERS, ROUTER_PROVIDERS, AuthService]);
//bootstrap(AppComponent, [provide(APP_CONFIG, { useValue: CONFIG }), HTTP_PROVIDERS, ROUTER_PROVIDERS, AuthService]);

