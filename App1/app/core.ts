﻿"use strict";

export module Core {
    
    // set of static methods useful throughout application    

    // determines if current runtime environment is browser emulator, e.g. ripple emulate | ionic serve
    export function isCordovaApp() {
        if (window.cordova /* or window['cordova'] if no /// <reference /> for cordova.d.ts */) { return true; } else { return false; }
        //return !!(window.cordova /* or window['cordova'] if no /// <reference /> for cordova.d.ts */);
    }

    // determines if current runtime environment is ripple emulator
    export function isBrowserEmulator() {
        if (window.parent && window.parent['ripple']) { return true; } else { return false; } // or return !!(window.parent && window.parent['ripple']);
        //if (window.tinyHippos != undefined) { return true; } else { return false; }
        //if (!!document.getElementById("tinyhippos-injected") && !!window.top.require) { return true; } else { return false; }
        //if (navigator.userAgent.match(/(Android|BlackBerry|Edge|IEMobile|iPad|iPhone|iPod)/)) { return false; } else { return true; }
// using window.parent['ripple'] in lieu of window.parent.ripple to get around typescript "Property 'ripple' does not exist on type 'Window'"
// ripple android  = Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36"
// ripple ios      = Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.109 Safari/537.36
// android google  = Mozilla/5.0 (Linux; Android 4.4.4; sdk Build/KK) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/33.0.0.0 Mobile Safari/537.36
// android vstudio = ???
// win bigscreen   = Mozilla/5.0 (Windows NT 10.0; Win64; x64; MSAppHost/3.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586
// win mobile      = ???
    }

    // extracted from https://github.com/MicrosoftDX/kurvejs/ | KurveIdentity.ts as it has cordova app environment ecmascript engine compatible story
    export function decodeAccessToken(accessToken: string): any {
        //let decodedToken = base64decode(accessToken.substring(accessToken.indexOf('.') + 1, accessToken.lastIndexOf('.')));
        let decodedToken = atob(accessToken.substring(accessToken.indexOf('.') + 1, accessToken.lastIndexOf('.')));
        let decodedTokenJson = JSON.parse(decodedToken);

        // get friendly date format of epoch/unix time value
        let expiryDate = new Date(new Date('01/01/1970 0:0 UTC').getTime() + parseInt(decodedTokenJson.exp) * 1000);

        return decodedTokenJson;
    }

    function base64decode(encodedString: string): string {  // solution for old browser ecmascript environments w/o atob support
        var e: any = {}, i: number, b = 0, c: number, x: number, l = 0, a: any, r = '', w = String.fromCharCode, L = encodedString.length;
        var A = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        for (i = 0; i < 64; i++) { e[A.charAt(i)] = i; }
        for (x = 0; x < L; x++) {
            c = e[encodedString.charAt(x)];
            b = (b << 6) + c;
            l += 6;
            while (l >= 8) {
                ((a = (b >>> (l -= 8)) & 0xff) || (x < (L - 2))) && (r += w(a));
            }
        }
        return r;
    }

    // extracted from https://www.npmjs.com/package/jwt-simple -> https://github.com/hokaccha/node-jwt-simple because couldn't get it to import
    // in systemjs module environment and same thing with https://www.npmjs.com/package/angular2-jwt -> https://github.com/auth0/angular2-jwt
    export function decodeAccessTokenAlt(token): any {
        // check token
        if (!token) {
            throw new Error('No token supplied');
        }
        // check segments
        var segments = token.split('.');
        if (segments.length !== 3) {
            throw new Error('Not enough or too many segments');
        }

        // All segment should be base64
        var headerSeg = segments[0];
        var payloadSeg = segments[1];
        var signatureSeg = segments[2];

        // base64 decode and parse JSON        
        //var header = JSON.parse(base64urlDecode(headerSeg));
        var header = JSON.parse(atob(headerSeg));
        //var payload = JSON.parse(base64urlDecode(payloadSeg));
        var payload = JSON.parse(atob(payloadSeg));

        return payload;
    };

    function base64urlDecode(str) {  // solution for node ecmascript engine environments w/o atob support
        return new Buffer(base64urlUnescape(str), 'base64').toString();
    }

    function base64urlUnescape(str) {
        str += new Array(5 - str.length % 4).join('=');
        return str.replace(/\-/g, '+').replace(/_/g, '/');
    }
}