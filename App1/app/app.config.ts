﻿// see aka.ms/ngPrv for details on how you eanble and use this ng2 equivalent to ConfigurationManager.AppSettings["<setting name>"]; for retrieving
// configuration settings from here or a dev wks file contained set of overrides

// In a.net execution environment [dnx] projects, e.g.asp.net 5 web app, it uses project.json to maintain settings that at runtime are accessed using 
// ConfigurationManager.AppSettings["<setting name>"]; and it supports dev wks overrides of those settings using a "userSecretsId": "MyDevWks" setting 
// which causes it to look for file at %appdata%\Microsoft\UserSecrets\MyDevWks\secrets.json for configuration settings overrides to use in f5 debug/test
// passes.

// In a .net runtime project, e.g.asp.net 4.5.2 web app and console app, it uses web.config and app.config respectively to maintain settings that at runtime 
// are accessed using ConfigurationManager.AppSettings["<setting name>"]; and it supports dev wks overrides of those settings using a 
// <appSettings file=<some local disk file path> /> attribute setting which causes it to look for file at that location for configuration settings overrides 
// to use in f5 debug/test passes.

export interface Config {
    test: string,
    apiEndpoint: string,
    authority: string,
    clientId: string,
    redirectUri: string,
    resourceUri: string,
    graphApiVersion: string,
    testAadAccessToken: string
}

export const CONFIG: Config = {
    test: 'dependency injection',
    apiEndpoint: 'api.mydomain.com',
    authority: 'https://login.windows.net/common',
    clientId: 'a5d92493-ae5a-4a9f-bcbf-9f1d354067d3',
    redirectUri: 'http://mydirectorysearcherapp',
    resourceUri: 'https://graph.windows.net',
    graphApiVersion: '2013-11-08',
    testAadAccessToken: 'abJ0eXAiOiJKV1 . . . 4321957'
};