﻿/// <binding />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

/// <binding Clean='clean' />
"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify");

//var babel = require('gulp-babel'),
//    traceur = require('gulp-traceur'),
//    plumber = require('gulp-plumber');

var paths = {
    webroot: "./www/"
};

paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";

//paths.es6jsfiles = paths.webroot + "app/es6/*.js";
//paths.es5js = paths.webroot + "app";

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:css"]);

gulp.task("min:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    return gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", ["min:js", "min:css"]);

gulp.task("copy:npmjs", function () {
    return gulp.src(['node_modules/es6-shim/**/*', 'node_modules/angular2/bundles/**/*', 'node_modules/systemjs/dist/**/*', 'node_modules/rxjs/bundles/**/*'],
        { base: 'node_modules' })
        .pipe(gulp.dest(paths.webroot + 'node_modules'));
});

gulp.task("copy", ["copy:npmjs"]);

//gulp.task('es6-to-es5-babel', function () {
//    gulp.src([paths.es6jsfiles])
//        .pipe(plumber())
//        .pipe(babel())
//        //.pipe(gulp.dest(paths.es5js + '/babel'));
//        .pipe(gulp.dest(paths.es5js));
//});

//gulp.task('es6-to-es5-traceur', function () {
//    gulp.src([paths.es6jsfiles])
//        .pipe(plumber())
//        .pipe(traceur({ blockbinding: true }))
//        .pipe(gulp.dest(paths.es5js + '/traceur'));
//});

//gulp.task('es6-watch', function () {
//    gulp.watch([paths.es6jsfiles], ['babel', 'traceur']);
//});
